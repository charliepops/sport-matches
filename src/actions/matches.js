import { saveData, deleteData, readData, updateData } from 'lib/firebase';

// save or overwrite match
export const saveMatch = match => async dispatch => {
  const key = await saveData('matches', match);
  dispatch({ type: 'MATCH', payload: { uid: key } });
};

// delete a match
export const deleteMatch = key => async dispatch => {
  await deleteData('matches/' + key);
  dispatch({ type: 'HOME' });
};

// add user to match
export const joinMatch = key => async (dispatch, getState) => {
  const { uid } = getState().profile.data;
  const ref = 'matches/' + key + '/joined';
  const joined = (await readData(ref)) || [];
  if (!joined.includes(uid)) {
    await updateData(ref, [...joined, uid]);
    dispatch({ type: 'MATCH_JOIN' });
  } else {
    dispatch({ type: 'MATCH_JOIN_NOT_INCLUDED' });
  }
};

// remove user from matches
export const leaveMatch = key => async (dispatch, getState) => {
  const { uid } = getState().profile.data;
  const ref = 'matches/' + key + '/joined';
  const joined = await readData(ref);
  const index = joined.indexOf(uid);
  if (index > -1) {
    joined.splice(index, 1);
    await updateData(ref, joined);
    dispatch({ type: 'MATCH_LEAVE' });
  } else {
    dispatch({ type: 'MATCH_LEAVE_NOT_FOUND' });
  }
};
