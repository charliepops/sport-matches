export const LOCATION_FOUND = 'LOCATION_FOUND';
export const LOCATION_NOT_FOUND = 'LOCATION_NOT_FOUND';
export const LOCATION_NOT_SUPPORTED = 'LOCATION_NOT_SUPPORTED';

export const findLocation = () => dispatch => {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      position => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        dispatch({
          type: LOCATION_FOUND,
          payload: pos
        });
      },
      () => {
        dispatch({
          type: LOCATION_NOT_FOUND,
          payload: 'not_found'
        });
      }
    );
  } else {
    dispatch({
      type: LOCATION_NOT_SUPPORTED,
      payload: 'not_supported'
    });
  }
};
