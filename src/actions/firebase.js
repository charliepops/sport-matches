import firebase from 'firebase';

export const FIREBASE_LOG_OUT = 'FIREBASE_LOG_OUT';

function login(provider) {
  firebase.auth().signInWithPopup(provider);
}

export const loginWithFacebook = () => {
  login(new firebase.auth.FacebookAuthProvider());
};

export const loginWithGoogle = () => {
  login(new firebase.auth.GoogleAuthProvider());
};

export const loginWithTwitter = () => {
  login(new firebase.auth.TwitterAuthProvider());
};

export const logout = () => {
  firebase.auth().signOut();
  return {
    type: FIREBASE_LOG_OUT
  };
};
