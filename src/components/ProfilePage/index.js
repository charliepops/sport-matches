import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Segment, Header, Image } from 'semantic-ui-react';

const ProfilePage = ({ profile }) => {
  const { data } = profile;

  return (
    <Container>
      <Segment>
        <Header>{data.displayName}</Header>
        <Image src={data.photoURL} />
      </Segment>
    </Container>
  );
};

ProfilePage.propTypes = {
  profile: PropTypes.object
};

export default connect(({ profile }) => ({ profile }))(ProfilePage);
