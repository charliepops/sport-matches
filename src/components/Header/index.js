import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'redux-first-router-link';
import { connect } from 'react-redux';
import { Menu, Button, Image, Icon } from 'semantic-ui-react';
import Login from 'components/Login';
import { logout } from 'actions';

class Header extends Component {
  onLogout = () => {
    this.props.dispatch(logout());
  };

  render() {
    const { profile } = this.props;
    return (
      <Menu>
        <Menu.Item header>
          <Link to="/">
            <Icon name="soccer" size="large" />
          </Link>
        </Menu.Item>
        <Menu.Menu position="right">
          {profile.isLogged && (
            <Menu.Item>
              <Link to="/create">
                <Button primary>Create Match</Button>
              </Link>
            </Menu.Item>
          )}
          {profile.isLogged && (
            <Menu.Item>
              <Link to="/profile">
                <Image
                  src={profile.data.photoURL}
                  size="mini"
                  verticalAlign="middle"
                  circular
                />
              </Link>
            </Menu.Item>
          )}
          <Menu.Item>
            {profile.isLogged ? (
              <Button onClick={this.onLogout}>Log out</Button>
            ) : (
              <Login />
            )}
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}

Header.propTypes = {
  profile: PropTypes.object,
  dispatch: PropTypes.func.isRequired
};

export default connect(({ profile }) => ({
  profile
}))(Header);
