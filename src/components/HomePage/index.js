import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Segment, Container, Header } from 'semantic-ui-react';
import Matches from './Matches';

class HomePage extends Component {
  render() {
    const { matches } = this.props;
    return (
      <Container>
        <Segment size="large">
          <Header as="h1">Matches</Header>
          <Matches matches={matches} />
        </Segment>
      </Container>
    );
  }
}

HomePage.propTypes = {
  profile: PropTypes.object,
  matches: PropTypes.object.isRequired
};

export default connect(({ profile, matches }) => ({ profile, matches }))(
  HomePage
);
