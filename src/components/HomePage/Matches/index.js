import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Table, Header, Button } from 'semantic-ui-react';
import Link from 'redux-first-router-link';
import { padZeros } from 'lib/utils';

export const Matches = ({ matches }) => {
  return matches.data ? (
    <Table celled fixed singleLine selectable textAlign="center">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Number of Players</Table.HeaderCell>
          <Table.HeaderCell>Location</Table.HeaderCell>
          <Table.HeaderCell>Date</Table.HeaderCell>
          <Table.HeaderCell>Time</Table.HeaderCell>
          <Table.HeaderCell />
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {Object.keys(matches.data).map(uid => {
          const {
            title,
            players,
            location,
            date,
            hour,
            minutes,
            daytime,
            joined
          } = matches.data[uid];
          const joinedTotal = joined ? joined.length : 0;
          return (
            <Table.Row key={uid}>
              <Table.Cell>{title}</Table.Cell>
              <Table.Cell>
                {joinedTotal} / {players}
              </Table.Cell>
              <Table.Cell>{location}</Table.Cell>
              <Table.Cell>{moment(date).format('ddd MMM D, YYYY')}</Table.Cell>
              <Table.Cell>{`${padZeros(hour)}:${padZeros(
                minutes
              )} ${daytime.toUpperCase()}`}</Table.Cell>
              <Table.Cell>
                <Link to={'/match/' + uid}>
                  <Button primary>Details</Button>
                </Link>
              </Table.Cell>
            </Table.Row>
          );
        })}
      </Table.Body>
    </Table>
  ) : (
    <Header>There are no matches available</Header>
  );
};

Matches.propTypes = {
  matches: PropTypes.object.isRequired
};

export default Matches;
