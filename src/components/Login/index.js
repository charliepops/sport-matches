import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Modal, Icon, Grid, Divider } from 'semantic-ui-react';
import { loginWithFacebook, loginWithGoogle, loginWithTwitter } from 'actions';

class LogIn extends Component {
  onFacebookClick = () => {
    this.props.dispatch(loginWithFacebook());
  };

  onGoogleClick = () => {
    this.props.dispatch(loginWithGoogle());
  };

  onTwitterClick = () => {
    this.props.dispatch(loginWithTwitter());
  };

  render() {
    return (
      <Modal size="tiny" trigger={<Button primary>Log in</Button>}>
        <Modal.Header>
          <h1 style={{ textAlign: 'center' }}>Log in</h1>
        </Modal.Header>
        <Modal.Content>
          <Grid centered>
            <Grid.Row>
              <Grid.Column width={6} textAlign="center">
                <Button fluid color="facebook" onClick={this.onFacebookClick}>
                  <Icon name="facebook" /> Facebook
                </Button>
                <Divider horizontal>Or</Divider>
                <Button secondary disabled fluid>
                  Sign up
                </Button>
                {/* <Button
                  disabled
                  fluid
                  color="twitter"
                  onClick={this.onGoogleClick}
                >
                  <Icon name="twitter" /> Twitter
                </Button>
                <p />
                <Button
                  disabled
                  fluid
                  color="google plus"
                  onClick={this.onGoogleClick}
                >
                  <Icon name="google" /> Google
                </Button> */}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Modal.Content>
      </Modal>
    );
  }
}

LogIn.propTypes = {
  dispatch: PropTypes.func.isRequired
};

export default connect()(LogIn);
