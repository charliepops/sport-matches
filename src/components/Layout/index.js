import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Header from 'components/Header';
import HomePage from 'components/HomePage';
import ProfilePage from 'components/ProfilePage';
import CreatePage from 'components/CreatePage';
import MatchPage from 'components/MatchPage';

const Layout = ({ route, profile }) => (
  <div>
    <Header profile={profile} />
    <main>
      {(route === 'home' || route === null) && <HomePage />}
      {route === 'create' && <CreatePage />}
      {route === 'edit' && <CreatePage />}
      {route === 'profile' && <ProfilePage />}
      {route === 'match' && <MatchPage />}
    </main>
  </div>
);

Layout.propTypes = {
  route: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  profile: PropTypes.object
};

export default connect(({ route, profile }) => ({ route, profile }))(Layout);
