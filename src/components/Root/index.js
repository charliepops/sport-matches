import PropTypes from 'prop-types';
import React from 'react';
import { Provider } from 'react-redux';
import Layout from 'components/Layout';

const Root = ({ store }) => (
  <Provider store={store}>
    <Layout />
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired
};

export default Root;
