import './index.scss';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Container,
  Segment,
  Header,
  Divider,
  Button,
  Message,
  Icon
} from 'semantic-ui-react';
import moment from 'moment';
import { connect } from 'react-redux';
import Link from 'redux-first-router-link';
import { padZeros } from 'lib/utils';
import DeleteModal from './DeleteModal';
import { deleteMatch, joinMatch, leaveMatch, fetchJoined } from 'actions';
import config from 'config';

class MatchPage extends Component {
  state = {
    isModalOpen: false
  };

  componentDidMount() {
    const { dispatch, match } = this.props;
    console.log(match);
    const { joined } = match;
    if (joined && joined.length) {
      dispatch(fetchJoined());
    }
  }

  onJoin = key => {
    this.props.dispatch(joinMatch(key));
  };

  onLeave = key => {
    this.props.dispatch(leaveMatch(key));
  };

  onDelete = key => {
    this.props.dispatch(deleteMatch(key));
    this.setState({ isModalOpen: false });
  };

  onShowModal = isOpen => {
    this.setState({ isModalOpen: isOpen });
  };

  render() {
    const { isModalOpen } = this.state;
    const { match, profile } = this.props;
    if (!match) {
      return (
        <Container>
          <Segment>
            <Header>{"This match doesn't exist"}</Header>
            <Link to="/">Go back to Home Page</Link>
          </Segment>
        </Container>
      );
    }
    const { isLogged } = profile;
    const {
      key,
      title,
      players,
      location,
      date,
      daytime,
      hour,
      minutes,
      userId,
      joined = [],
      position
    } = match;
    const joinedTotal = joined.length;
    const isComplete = joinedTotal === Number(players);
    return (
      <Grid centered columns={1}>
        <Grid.Column width={8}>
          <Segment>
            <Header as="h1">{title}</Header>
            <span>
              {`${moment(date).format('dddd MMM DD, YYYY')}`}{' '}
              {`${padZeros(hour)}:${padZeros(
                minutes
              )} ${daytime.toUpperCase()}`}
            </span>
            <Divider />
            <Header as="h3">Going ({joinedTotal + ' / ' + players})</Header>
            {joinedTotal &&
              joined.map(user => (
                <Icon key={user} circular name="user circle outline" />
              ))}
            <br />
            <Header as="h3">Location</Header>
            <Header.Subheader>{location}</Header.Subheader>
            <br />
            <div className="map-wrapper">
              <a
                target="_blank"
                href={`https://www.google.com/maps/dir/?api=1&destination=${
                  position.lat
                },${position.lng}`}
              >
                <img
                  src={`https://maps.googleapis.com/maps/api/staticmap?center=${
                    position.lat
                  },${position.lng}&zoom=16&size=630x300&key=${
                    config.gmaps.apiKey
                  }`}
                />
                <Icon name="marker" size="big" className="map-marker" />
              </a>
            </div>
            {isComplete && <Message color="brown">This Match is full</Message>}
            <Divider />
            {isLogged &&
              joined.includes(profile.data.uid) && (
                <Button
                  secondary
                  size="big"
                  onClick={this.onLeave.bind(this, key)}
                >
                  Leave
                </Button>
              )}
            {!isComplete &&
              isLogged &&
              !joined.includes(profile.data.uid) && (
                <Button
                  primary
                  size="big"
                  onClick={this.onJoin.bind(this, key)}
                >
                  Join
                </Button>
              )}
            {isLogged &&
              profile.data.uid === userId && (
                <span>
                  <DeleteModal
                    onDelete={this.onDelete.bind(this, key)}
                    onOpen={this.onShowModal.bind(this, true)}
                    onClose={this.onShowModal.bind(this, false)}
                    isOpen={isModalOpen}
                  />
                  <Link to={'/edit/' + key}>
                    <Button floated="right" color="grey" size="big">
                      Edit
                    </Button>
                  </Link>
                </span>
              )}
            {!isLogged && (
              <Message>you must be logged in to join this match</Message>
            )}
          </Segment>
        </Grid.Column>
      </Grid>
    );
  }
}

MatchPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired
};

export default connect(({ matches, location, profile }) => {
  const match = matches.data[location.payload.uid];
  return {
    match,
    profile
  };
})(MatchPage);
