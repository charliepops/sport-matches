import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'semantic-ui-react';

const DeleteModal = ({ onDelete, onClose, isOpen, onOpen }) => (
  <Modal
    open={isOpen}
    size="tiny"
    trigger={
      <Button floated="right" onClick={onOpen} negative size="big">
        Delete
      </Button>
    }
  >
    <Modal.Header>Are you sure you want to delete this match</Modal.Header>
    <Modal.Actions>
      <Button negative onClick={onDelete}>
        Delete
      </Button>
      <Button primary onClick={onClose}>
        Cancel
      </Button>
    </Modal.Actions>
  </Modal>
);

DeleteModal.propTypes = {
  onDelete: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  onOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired
};

export default DeleteModal;
