import './index.scss';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

class Location extends Component {
  mapLoaded = false;

  constructor(props) {
    super(props);
    const location = this.props.location || '';
    this.state = {
      location
    };
  }

  componentDidMount() {
    this.loadMap();
  }

  componentDidUpdate() {
    this.loadMap();
  }

  loadMap = () => {
    const position = isEmpty(this.props.position)
      ? this.props.profile.position
      : this.props.position;
    if (position && !this.mapLoaded) {
      this.mapLoaded = true;
      this.map = new google.maps.Map(this.wrapper, {
        zoom: 16,
        disableDefaultUI: true,
        zoomControl: true,
        clickableIcons: false
      });
      this.updatePosition(position);
      this.initAutocomplete();
      this.map.addListener('idle', this.onCenterChange);
    }
  };

  onCenterChange = () => {
    const center = this.map.getCenter();
    if (!center) return;
    const position = {
      lat: center.lat(),
      lng: center.lng()
    };
    this.props.onChange({ location: this.state.location, position });
  };

  onSearchChange = event => {
    this.setState({ location: event.target.value });
  };

  updatePosition(position) {
    this.map.setCenter(position);
  }

  initAutocomplete() {
    this.autocomplete = new google.maps.places.Autocomplete(
      this.searchLocation,
      {
        types: ['geocode']
      }
    );
    this.autocomplete.bindTo('bounds', this.map);
    this.autocomplete.addListener('place_changed', this.fillInAddress);
  }

  fillInAddress = () => {
    const { geometry, formatted_address } = this.autocomplete.getPlace();
    this.updatePosition(geometry.location);
    this.setState({ location: formatted_address });
  };

  render() {
    const { location } = this.state;
    return (
      <div className="location">
        <input
          name="location-search"
          placeholder="Search area"
          value={location}
          onChange={this.onSearchChange}
          ref={el => {
            this.searchLocation = el;
          }}
        />
        <div className="location-map-wrapper">
          <div
            className="location-map"
            ref={el => {
              this.wrapper = el;
            }}
          />
          <Icon name="marker" size="big" className="location-marker" />
        </div>
      </div>
    );
  }
}

Location.propTypes = {
  profile: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  position: PropTypes.object.isRequired,
  location: PropTypes.string.isRequired
};

export default connect(({ profile }) => ({ profile }))(Location);
