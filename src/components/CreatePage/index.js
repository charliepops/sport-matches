import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import 'react-day-picker/lib/style.css';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import moment from 'moment';
import { Grid, Header, Form, Message, Segment } from 'semantic-ui-react';
import { saveMatch } from 'actions';
import { padZeros } from 'lib/utils';
import Location from './Location';

class CreatePage extends Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState(props);
  }

  getInitialState(props) {
    const { params: { uid }, matches } = props;
    let formData = {
      error: '',
      loading: false,
      isEditing: false,
      form: {
        title: '',
        players: 10,
        location: '',
        position: {},
        date: Date.now(),
        hour: '12',
        minutes: '0',
        daytime: 'pm',
        isPrivate: false
      }
    };
    if (uid) {
      formData = { ...formData, form: matches.data[uid], isEditing: true };
    }
    return formData;
  }

  onChange = event => {
    this.setState({
      error: '',
      form: {
        ...this.state.form,
        [event.target.name]: event.target.value
      }
    });
  };

  onDateChange = day => {
    this.setState({
      error: '',
      form: { ...this.state.form, date: moment(day).valueOf() }
    });
  };

  onDaytimeChange = (event, data) => {
    this.setState({
      error: '',
      form: { ...this.state.form, daytime: data.value }
    });
  };

  onPrivateChange = () => {
    this.setState({
      form: { ...this.state.form, isPrivate: !this.state.form.isPrivate }
    });
  };

  onLocationChange = ({ location, position }) => {
    this.setState({
      form: { ...this.state.form, location, position }
    });
  };

  async onSubmit(event) {
    event.preventDefault();
    const { profile } = this.props;
    const { form, isEditing } = this.state;
    this.setState({ loading: true, error: '' });
    if (this.isFormValid()) {
      const data = { ...form, userId: profile.data.uid };
      if (!isEditing) {
        data.dateCreated = Date.now();
      }
      await this.props.dispatch(saveMatch(data));
    } else {
      this.setState({ error: 'All fields are required', loading: false });
    }
  }

  isFormValid() {
    const { form } = this.state;
    const invalid = Object.keys(form).every(field => {
      return field === 'isPrivate' || !!form[field];
    });
    return invalid;
  }

  render() {
    const {
      form: {
        title,
        players,
        date,
        daytime,
        hour,
        minutes,
        isPrivate,
        position,
        location
      },
      loading,
      error,
      isEditing
    } = this.state;
    // dropdown options
    const options = [
      { key: 'am', text: 'AM', value: 'am' },
      { key: 'pm', text: 'PM', value: 'pm' }
    ];
    return (
      <Grid centered columns={1}>
        <Grid.Column width={8}>
          <Segment>
            <Header as="h1">Create Match</Header>
            {!!error && <Message error content={error} />}
            <Form loading={loading} onSubmit={this.onSubmit.bind(this)}>
              <Form.Field>
                <Form.Input
                  label="Name"
                  placeholder="Name or short description"
                  value={title}
                  name="title"
                  onChange={this.onChange}
                />
              </Form.Field>
              <Form.Field inline>
                <Form.Input
                  icon="users"
                  type="number"
                  label="Limit number of players"
                  min="2"
                  max="99"
                  value={players}
                  name="players"
                  onChange={this.onChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Date and Time</label>
              </Form.Field>
              <Form.Group inline>
                <Form.Field>
                  <DayPickerInput
                    name="date"
                    placeholder="MMM DD, YYYY"
                    format="MMM DD, YYYY"
                    value={moment(date).format('dddd MMM DD, YYYY')}
                    onDayChange={this.onDateChange}
                    dayPickerProps={{
                      disabledDays: {
                        before: new Date()
                      }
                    }}
                  />
                </Form.Field>
                <Form.Input
                  name="hour"
                  type="number"
                  placeholder="12"
                  value={padZeros(hour)}
                  min="1"
                  max="12"
                  onChange={this.onChange}
                />
                <Form.Input
                  name="minutes"
                  type="number"
                  placeholder="00"
                  value={padZeros(minutes)}
                  min="0"
                  max="59"
                  onChange={this.onChange}
                />
                <Form.Select
                  compact
                  options={options}
                  placeholder="AM/PM"
                  value={daytime}
                  onChange={this.onDaytimeChange}
                />
              </Form.Group>
              <Form.Field>
                <label>Location</label>
                <Location
                  position={position}
                  location={location}
                  onChange={this.onLocationChange}
                />
              </Form.Field>
              <Form.Checkbox
                checked={isPrivate}
                name="isPrivate"
                onChange={this.onPrivateChange}
                label="Private match"
              />
              <Form.Field>
                <Form.Button type="submit" primary size="big">
                  {isEditing ? 'Save' : 'Create'}
                </Form.Button>
              </Form.Field>
            </Form>
          </Segment>
        </Grid.Column>
      </Grid>
    );
  }
}

CreatePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
  matches: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired
};

export default connect(({ matches, location, profile }) => ({
  matches,
  params: location.payload,
  profile
}))(CreatePage);
