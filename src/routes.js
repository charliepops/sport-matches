import { redirect } from 'redux-first-router';

const userOnly = (dispatch, getState) => {
  const { isLogged } = getState().profile;
  if (!isLogged) {
    const action = redirect({ type: 'HOME' });
    dispatch(action);
  }
};

export default {
  HOME: '/',
  PROFILE: {
    path: '/profile',
    thunk: userOnly
  },
  CREATE: {
    path: '/create',
    thunk: userOnly
  },
  EDIT: {
    path: '/edit/:uid',
    thunk: userOnly
  },
  MATCH: '/match/:uid'
};
