import { LOCATION_FOUND } from 'actions';

export default (
  state = { data: null, isLogged: false, position: null },
  action
) => {
  switch (action.type) {
    case 'PROFILE_CHANGE':
      return {
        ...state,
        data: action.payload.data,
        isLogged: !!action.payload.data
      };
    case LOCATION_FOUND:
      return {
        ...state,
        position: action.payload
      };
    default:
      return state;
  }
};
