export default (state = {}, action) => {
  switch (action.type) {
    case 'USERS_SYNC':
      return { ...state, data: action.payload.data };
    default:
      return state;
  }
};
