import { NOT_FOUND } from 'redux-first-router';

export default (state = null, action = {}) => {
  switch (action.type) {
    case 'HOME':
      return 'home';
    case 'PROFILE':
      return 'profile';
    case 'CREATE':
      return 'create';
    case 'EDIT':
      return 'edit';
    case 'MATCH':
      return 'match';
    case NOT_FOUND:
      return null;
    default:
      return state;
  }
};
