import { isEmpty } from 'lodash';

export default (state = { data: null }, action) => {
  switch (action.type) {
    case 'MATCHES_SYNC':
      return { ...state, data: action.payload.data };
    case 'MATCHES_CHILD_ADDED':
      return {
        ...state,
        data: { ...state.data, [action.payload.data.key]: action.payload.data }
      };
    case 'MATCHES_CHILD_REMOVED':
      delete state.data[action.payload.data.key];
      if (isEmpty(state.data)) state.data = null;
      return { ...state };
    case 'MATCHES_CHILD_CHANGED':
      state.data[action.payload.data.key] = action.payload.data;
      return { ...state };
    default:
      return state;
  }
};
