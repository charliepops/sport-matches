import config from 'config';
import firebase from 'firebase';
import { syncFirebaseRef } from './firebase/sync';

export const initializeFirebase = () => {
  firebase.initializeApp(config.firebase);
};

// sync user firebase user
export const syncFirebaseUser = store => {
  return new Promise(resolve => {
    firebase.auth().onAuthStateChanged(async user => {
      const data = user ? user.providerData[0] : null;
      store.dispatch({
        type: 'PROFILE_CHANGE',
        payload: {
          data
        }
      });
      resolve();
    });
  });
};

// save user provider data
export const saveProfileData = data => {
  const { displayName, email, photoURL, uid } = data;
  return new Promise(resolve => {
    firebase
      .database()
      .ref('users')
      .child(uid)
      .set({
        displayName,
        email,
        photoURL,
        uid
      })
      .then(resolve);
  });
};

// create or overwrite key reference
export const saveData = (ref, data) => {
  return new Promise(resolve => {
    if (!data.key) {
      data.key = firebase
        .database()
        .ref(ref)
        .push()
        .key.substr(1);
    }
    firebase
      .database()
      .ref(ref)
      .child(data.key)
      .set(data)
      .then(() => resolve(data.key));
  });
};

// update reference
export const updateData = (ref, data) => {
  new Promise(function(resolve) {
    firebase
      .database()
      .ref(ref)
      .set(data)
      .then(resolve);
  });
};

// delete reference
export const deleteData = ref => {
  return new Promise(resolve => {
    firebase
      .database()
      .ref(ref)
      .remove()
      .then(resolve);
  });
};

// read reference
export const readData = ref => {
  return new Promise(resolve => {
    firebase
      .database()
      .ref(ref)
      .once('value')
      .then(snapshot => resolve(snapshot.val()));
  });
};

// sync firebase changes with redux store
export const syncFirebaseData = async store => {
  await syncFirebaseRef('matches', store);
  // await syncFirebaseRef('users', store);
};
