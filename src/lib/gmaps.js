import config from 'config';

const body = document.body;
const script = document.createElement('script');
script.async = true;
script.defer = true;
script.src =
  'https://maps.googleapis.com/maps/api/js?key=' +
  config.gmaps.apiKey +
  '&libraries=places';
body.appendChild(script);
