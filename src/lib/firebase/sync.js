import firebase from 'firebase';

const eventTypes = [
  'child_added',
  'child_removed',
  'child_changed',
  'child_moved'
];

// used to make sure first 'value' event finished
// before adding child handlers
let refSynced = [];

const syncChildren = (ref, store) => {
  const fref = firebase.database().ref(ref);
  eventTypes.forEach(event => {
    fref.on(event, snapshot => {
      // make sure it was initialy synced
      if (refSynced.includes(ref)) {
        const type =
          ref
            .toUpperCase()
            .split('/')
            .pop() +
          '_' +
          event.toUpperCase();
        store.dispatch({
          type,
          payload: {
            data: snapshot.val()
          }
        });
      }
    });
  });
};

// sync initial store
const syncValue = (ref, store) => {
  return new Promise(resolve => {
    firebase
      .database()
      .ref(ref)
      .once('value', snapshot => {
        refSynced.push(ref);
        store.dispatch({
          type: ref.toUpperCase() + '_SYNC',
          payload: {
            data: snapshot.val()
          }
        });
        resolve();
      });
  });
};

export const syncFirebaseRef = async (ref, store) => {
  syncChildren(ref, store);
  // initial sync only
  await syncValue(ref, store);
};
