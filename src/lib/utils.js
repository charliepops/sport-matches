export const padZeros = (value, before = 10) =>
  value < before ? 0 + value : value;
