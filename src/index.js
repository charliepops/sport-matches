import 'semantic-ui-css/semantic.css';
import 'styles/index.scss';
import 'babel-polyfill';
import 'lib/gmaps';
import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Root from 'components/Root';
import { configureStore, initialDispatch } from 'configureStore.js';
import {
  initializeFirebase,
  syncFirebaseData,
  syncFirebaseUser
} from 'lib/firebase';
import { findLocation } from 'actions';

const store = configureStore();

(async () => {
  initializeFirebase();
  await syncFirebaseUser(store);
  await syncFirebaseData(store);
  initialDispatch();
  renderApp(Root);
  store.dispatch(findLocation());
})();

const renderApp = Root => {
  ReactDOM.render(
    <AppContainer>
      <Root store={store} />
    </AppContainer>,
    document.getElementById('app-container')
  );
};

if (module.hot) {
  module.hot.accept('components/Root', () => {
    const newRoot = require('components/Root').default;
    renderApp(newRoot);
  });
}
