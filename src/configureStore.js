import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { connectRoutes } from 'redux-first-router';
import createHistory from 'history/createBrowserHistory';
import * as reducers from 'reducers';
import routes from 'routes';

const history = createHistory();
const {
  reducer,
  middleware,
  enhancer,
  initialDispatch
} = connectRoutes(history, routes, {
  initialDispatch: false
});

const rootReducer = combineReducers({ ...reducers, location: reducer });

let middlewares = [thunk, middleware];
if (process.env.NODE_ENV === 'development') {
  middlewares = [...middlewares, createLogger({ collapsed: true })];
}

const configureStore = () => {
  const store = createStore(
    rootReducer,
    compose(enhancer, applyMiddleware(...middlewares))
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducers', () => {
      const nextRootReducer = require('./reducers/index');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};

export { configureStore, initialDispatch };
