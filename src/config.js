const config = {
  development: {
    firebase: {
      apiKey: 'AIzaSyDyQcsyInH_kDuAkD5-m4FYiwEyB3BAK-s',
      authDomain: 'sport-matches-development.firebaseapp.com',
      databaseURL: 'https://sport-matches-development.firebaseio.com',
      projectId: 'sport-matches-development',
      storageBucket: 'sport-matches-development.appspot.com',
      messagingSenderId: '621543756397'
    },
    gmaps: {
      apiKey: 'AIzaSyB9lTie0g8ZpXABPhSDuzhlPyBuMoMU0cw'
    }
  },
  production: {
    firebase: {
      apiKey: 'AIzaSyB4awwow86C-oFiRMGpvpwVt1VWSUXn3hE',
      authDomain: 'sport-matches.firebaseapp.com',
      databaseURL: 'https://sport-matches.firebaseio.com',
      projectId: 'sport-matches',
      storageBucket: 'sport-matches.appspot.com',
      messagingSenderId: '883606004265'
    },
    gmaps: {
      apiKey: 'AIzaSyB9lTie0g8ZpXABPhSDuzhlPyBuMoMU0cw'
    }
  }
};

export default config[process.env.NODE_ENV];
