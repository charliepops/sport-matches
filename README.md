# Matches

An application to create and share sport matches.

## Usage

* Run `npm start` to compile source files and run development server.
* Run `npm run build:dev` to create development static files in **build**
  directory.
* Run `npm run build:prod` to create production static files in **build**
  directory.
* Run `npm run build:serve` to serve latest build.
* Run `npm run deploy:dev` deploy build to development.
* Run `npm run deploy:prod` deploy build to production.
